package main

type (
	State struct {
		StateID            int `gorm:"primary_key; unique_index"`
		StateName          string
		ThreeCharStateCode string
		ISOCode            string
		CountryID          int `gorm:"foreign_key:CountryID"`
	}
	Country struct {
		CountryID            int `gorm:"primary_key; unique_index"`
		CountryName          string
		TwoCharCountryCode   string
		ThreeCharCountryCode string
	}
)

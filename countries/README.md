## Countries SQL dump
### Instruction
1. Create Tables. Or use structs in ```schema.go``` if you're using gorm
```sql
CREATE TABLE IF NOT EXISTS public.countries
(
    country_id integer NOT NULL DEFAULT nextval('countries_country_id_seq'::regclass),
    country_name text COLLATE pg_catalog."default",
    two_char_country_code text COLLATE pg_catalog."default",
    three_char_country_code text COLLATE pg_catalog."default",
    CONSTRAINT countries_pkey PRIMARY KEY (country_id)
);

CREATE TABLE IF NOT EXISTS public.states
(
    state_id integer NOT NULL DEFAULT nextval('states_state_id_seq'::regclass),
    state_name text COLLATE pg_catalog."default",
    three_char_state_code text COLLATE pg_catalog."default",
    iso_code text COLLATE pg_catalog."default",
    country_id integer,
    CONSTRAINT states_pkey PRIMARY KEY (state_id),
    CONSTRAINT states_country_id_countries_country_id_foreign FOREIGN KEY (country_id)
        REFERENCES public.countries (country_id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE CASCADE
);
```

2. Run SQL in ```countries.sql``` then ```states_mys.sql```

